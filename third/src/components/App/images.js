import MinRickAndMorty from '../../images/rick-and-morty_min.jpg';
import MinRickAndMorty2 from '../../images/rick-and-morty2_min.jpg';
import MinOverTheGardenWall from '../../images/over-the-garden-wall_min.jpg';
import MinRandom1 from '../../images/random1_min.jpg';
import MinGolang from '../../images/golang_min.jpg';
import MinBauhaus from '../../images/bauhaus_min.jpg';
import MinHermesTrismegisto from '../../images/hermes-trismegisto_min.jpg';
import MinKandinsky from '../../images/kandinsky_min.jpg';
import MinMurallasRojas from '../../images/murallas-rojas_min.jpg';
import MinInhotim from '../../images/inhotim_min.jpg';
import MinAdoniran from '../../images/adoniran_min.jpg';
import MinGoogle from '../../images/google_min.jpg';

const images = [
  {
    src: '/static/murallas-rojas.jpg',
    placeholder: MinMurallasRojas,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/rick-and-morty.jpg',
    placeholder: MinRickAndMorty,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/bauhaus.jpg',
    placeholder: MinBauhaus,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/golang.jpg',
    placeholder: MinGolang,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/kandinsky.jpg',
    placeholder: MinKandinsky,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/adoniran.jpg',
    placeholder: MinAdoniran,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/random1.jpg',
    placeholder: MinRandom1,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/inhotim.jpg',
    placeholder: MinInhotim,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/rick-and-morty2.jpg',
    placeholder: MinRickAndMorty2,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/google.jpg',
    placeholder: MinGoogle,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/hermes-trismegisto.jpg',
    placeholder: MinHermesTrismegisto,
    width: '400px',
    height: '400px',
  },
  {
    src: '/static/over-the-garden-wall.jpg',
    placeholder: MinOverTheGardenWall,
    width: '400px',
    height: '400px',
  },
];

export default images;
