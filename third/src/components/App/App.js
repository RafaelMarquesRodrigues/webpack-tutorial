import React from 'react';

import {
  Wrapper,
} from './styled';

import ImageLoader from '../ImageLoader';
import Loader from '../Loader';

import images from './images';

class App extends React.Component {
  state = {
    loaded: [],
  };

  handleImageLoaded = src => this.setState(state => ({ loaded: [...state.loaded, src] }));

  render() {
    const { loaded } = this.state;

    return (
      <React.Fragment>
        {
          loaded.length !== images.length ? <Loader /> : <h1 style={{ textAlign: 'center' }}>done</h1>
        }
        <Wrapper>
          {
            images.map(img => (
              <ImageLoader
                key={img.src}
                src={img.src}
                placeholder={img.placeholder}
                width={img.width}
                height={img.height}
                onImageLoaded={this.handleImageLoaded}
              />
            ))
          }
        </Wrapper>
      </React.Fragment>
    );
  }
}

export default App;
