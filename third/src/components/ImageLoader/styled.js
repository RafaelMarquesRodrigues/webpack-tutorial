import styled, { keyframes } from 'styled-components';

const cubicBezier = 'cubic-bezier(.34, .22, .32, .89)';

const entering = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1.0;
  }
`;

export const ImageWrapper = styled.div`
  box-sizing: border-box;
  position: relative;
  height: ${props => props.height || '100%'};
  width: ${props => props.width || '100%'};
`;

export const FadeImage = styled.img`
  object-fit: contain;
  animation: ${entering} .5s ${cubicBezier};
  visibility: ${props => (props.min && props.loaded ? 'hidden' : 'visible')};
  transition: visibility .5s ${cubicBezier}, filter .5s ${cubicBezier};
  filter: ${props => (props.loaded ? 'none' : 'blur(15px)')};
  position: absolute;
`;
