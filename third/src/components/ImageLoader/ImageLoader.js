import React from 'react';
import PropTypes from 'prop-types';

import {
  FadeImage, ImageWrapper,
} from './styled';

const propTypes = {
  src: PropTypes.string.isRequired,
  onImageLoaded: PropTypes.func,
  placeholder: PropTypes.string.isRequired,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  width: null,
  height: null,
  onImageLoaded: () => false,
};

class ImageLoader extends React.Component {
  state = {
    srcImg: '',
    loaded: false,
    loading: true,
  };

  componentDidMount() {
    const img = new Image();
    const { src, onImageLoaded } = this.props;
    img.src = src;
    img.onload = () => {
      this.setState({ srcImg: img.src, loaded: true }, onImageLoaded(img.src));
    };
  }

  handlePlaceholderLoaded = () => this.setState({ loading: false });

  render() {
    const { srcImg, loaded, loading } = this.state;
    const { placeholder, width, height } = this.props;

    return (
      <ImageWrapper
        width={width}
        height={height}
      >
        <FadeImage
          src={placeholder}
          loaded={loaded}
          onLoad={this.handlePlaceholderLoaded}
          min="true"
          width={width}
          height={height}
        />
        {
          !loading && loaded && (
            <FadeImage
              src={srcImg}
              loaded={loaded}
              width={width}
              height={height}
            />
          )
        }
      </ImageWrapper>
    );
  }
}

ImageLoader.propTypes = propTypes;
ImageLoader.defaultProps = defaultProps;

export default ImageLoader;
