import React from 'react';
import styled, { keyframes } from 'styled-components';

const entering = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

const Root = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: center;
`;

const Wrapper = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  animation: ${entering} .3s linear;
`;

class CommentList extends React.Component {
  state = {
    max: 10,
    loading: false,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = event => {
    if (!this.state.loading && (window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.setState({ loading: true }, () => {
        setTimeout(() => {
          this.setState({ max: this.state.max + 5, loading: false });
        }, 1000);
      })
    }
  };

  render() {
    const { comments } = this.props;
    const { max, loading } = this.state;

    return (
      <Root>
        { !comments && <h1>nothing to show</h1>}
        {
          comments && comments.slice(0, max).map((c, index) => (
            <Wrapper key={c.name} delay={index%5}>
              <h1>{c.name}</h1>
              <p>{c.email}</p>
              <span>{c.span}</span>
            </Wrapper>
          ))
        }
        { loading && <h1>L O A D I N G . . .</h1>}
      </Root>
    );
  }
}

export default CommentList;
