import React from 'react';
import Link from 'next/link'
import { Root } from './styled';
import { Button, routes } from '../shared';

const NavLink = ({ path, label }) =>
(
  <Link href={path}>
    <Button>{label}</Button>
  </Link>
);

const Navigation = () =>
(
  <Root>
    {
      routes.map(route => <NavLink {...route} key={route.path}/>)
    }
  </Root>
);

export default Navigation;
