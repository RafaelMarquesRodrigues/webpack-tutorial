import React from 'react';

import { Wrapper, PageWrapper } from './styled';

import Navigation from '../Navigation';

const Layout = props =>
(
  <Wrapper>
    <Navigation />
    <PageWrapper>
      {props.children}
    </PageWrapper>
  </Wrapper>
);

export default Layout;
