import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: inline;
`;

export const PageWrapper = styled.div`
  margin-top: 100px;
`;