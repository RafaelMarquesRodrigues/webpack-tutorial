import React from 'react';
import styled from 'styled-components';

import ImageLoader from './ImageLoader';
import { Root } from './styled';

class ImagesList extends React.Component {
  state = {
    max: 20,
    loading: false,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = event => {
    if (!this.state.loading && (window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.setState({ loading: true }, () => {
        setTimeout(() => {
          this.setState({ max: this.state.max + 10, loading: false });
        }, 1000);
      })
    }
  };

  render() {
    const { images } = this.props;
    const { max, loading } = this.state;

    return (
      <Root>
        { !images && <h1>nothing to show</h1>}
        {
          images && images.slice(0, max).map(img =>
            <ImageLoader
              key={img.id}
              src={img.url}
              placeholder={img.thumbnailUrl}
              width="300px"
              height="300px"
            />
          )
        }
        { loading && <h1>L O A D I N G . . .</h1>}
      </Root>
    );
  }
}

export default ImagesList;
