const HOME = {
  path: '/',
  label: 'home'
};

const OTHER = {
  path: '/other',
  label: 'other'
};

const ANOTHER = {
  path: '/another',
  label: 'comments'
};

const IMAGES = {
  path: '/images',
  label: 'IMG'
};

const routes = [HOME, OTHER, ANOTHER, IMAGES];

export default routes;
