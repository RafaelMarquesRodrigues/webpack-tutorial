import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 42px;
  cursor: pointer;
  color: #FFF;
  background: orange;
  border-radius: 5px;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  font-family: 'Montserrat', sans-serif;
`;

const Button = (props) =>
(
  /* NEXT LINK PASS ONCLICK FROM LINK AS PROPS */
  <StyledButton onClick={props.onClick}>
    {props.children}
  </StyledButton>
)

export default Button;
