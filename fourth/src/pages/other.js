import React from 'react';

import dynamic from 'next/dynamic';

const Component = dynamic(() => import('../components/MaterialComponent'), {
  loading: () => <p>i'm loading...</p>
});

export default () =>
(
  <React.Fragment>
    <p>other</p>
    <Component />
  </React.Fragment>
);