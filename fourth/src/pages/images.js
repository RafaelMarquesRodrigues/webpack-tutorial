import React from 'react';
import fetch from 'isomorphic-unfetch';
import ImagesList from '../components/ImagesList';

const Images = props =>
(
  <React.Fragment>
    <ImagesList {...props} />
  </React.Fragment>
);

Images.getInitialProps = async () => {
  const json = await fetch('https://jsonplaceholder.typicode.com/photos').then(r => r.json());
  return { images: json };
}

export default Images;
