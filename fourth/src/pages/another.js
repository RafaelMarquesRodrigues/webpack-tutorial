import React from 'react';
import fetch from 'isomorphic-unfetch';
import CommentList from '../components/CommentList';

const Another = props =>
(
  <React.Fragment>
    <CommentList {...props} />
  </React.Fragment>
);

Another.getInitialProps = async () => {
  const json = await fetch('https://jsonplaceholder.typicode.com/comments').then(r => r.json());
  return { comments: json };
}

export default Another;
