const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders }) => {

    if(!isServer && isProd) {
      config.plugins.push(new BundleAnalyzerPlugin({
        analyzerMode: 'static',
      }));
    }
    /*
    */
    return config;
  }
}