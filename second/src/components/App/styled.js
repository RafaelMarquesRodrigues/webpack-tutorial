import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const CoolText = styled.h1`
  font-family: 'Monoton', cursive;
  font-size: 40px;
`;