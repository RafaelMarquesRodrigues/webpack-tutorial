import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { injectGlobal } from 'styled-components';

injectGlobal`
  html {
    width: 100vw;
    height: 100vh;
  }
  body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

ReactDOM.render(<App />, document.getElementById('root'));
